# 用户中心

## 项目流程

需求分析 => 设计（概要设计、详细设计）=> 技术选型 => 初始化 / 引入需要的技术 => 写 Demo => 写代码（实现业务逻辑） => 测试（单元测试、系统测试）=> 代码提交 / 代码评审 => 部署 => 发布上线



## 需求分析

1. 登录 / 注册
2. 用户管理（仅管理员可见）对用户的查询或者修改
3. 用户校验（仅星球用户可见）



## 技术选型

前端：三件套 + React + 组件库 Ant Design + Umi + Ant Design Pro（现成的管理系统）

后端：

- java
- spring（依赖注入框架，帮助你管理 Java 对象，集成一些其他的内容）
- springmvc（web 框架，提供接口访问、restful接口等能力）
- mybatis（Java 操作数据库的框架，持久层框架，对 jdbc 的封装）
- mybatis-plus（对 mybatis 的增强，不用写 sql 也能实现增删改查）
- springboot（**快速启动** / 快速集成项目。不用自己管理 spring 配置，不用自己整合各种框架）
- junit 单元测试库
- mysql 数据库

部署：服务器 / 容器（平台）

### 1.创建后端项目

项目需要用到的依赖

![image-20231109131928072](https://cdncode.oss-cn-beijing.aliyuncs.com/test/202312292336691.png)

### 2.数据库设计（:o:踩坑  :new:新插件     ）

```sql

-- 切换库
use jiefencenter;

# 用户表
create table user
(
    user_name     varchar(256)                       null comment '用户昵称',
    id           bigint auto_increment comment 'id'
        primary key,
    user_account  varchar(256)                       null comment '账号',
    avatar_url    varchar(1024)                      null comment '用户头像',
    gender       tinyint                            null comment '性别',
    user_password varchar(512)                       not null comment '密码',
    phone        varchar(128)                       null comment '电话',
    email        varchar(512)                       null comment '邮箱',
    user_status   int      default 0                 not null comment '状态 0 - 正常',
    create_time   datetime default CURRENT_TIMESTAMP null comment '创建时间',
    update_time   datetime default CURRENT_TIMESTAMP null on update CURRENT_TIMESTAMP,
    is_delete     tinyint  default 0                 not null comment '是否删除',
    user_role     int      default 0                 not null comment '用户角色 0 - 普通用户 1 - 管理员',
    planet_code   varchar(512)                       null comment '星球编号'
)
    comment '用户';
```

:smiling_imp:

**注意**：数据库字段使用**下划线命**名时，配置字段时候无需配置application.yml，使用插件也无需勾选Actual Column

**配置文件：**

```
#my-batis-plus驼峰配置 当数据库字段中有下划线是时，采用驼峰式命名，默认开启
#mybatis-plus:
#  configuration:
#    map-underscore-to-camel-case: fals
```

  **:new:   my-batis-x插件使用：**

![image-20231112213358910](https://cdncode.oss-cn-beijing.aliyuncs.com/test/202311122148456.png)

:smiling_imp:

**注意**：数据库字段使用**大小写**名时，配置字段时候需要配置application.yml

```
#my-batis-plus驼峰配置 当数据库字段中有下划线是时，采用驼峰式命名，默认开启
#mybatis-plus:
#  configuration:
#    map-underscore-to-camel-case: fals
```



**:new:   my-batis-x插件使用**

![image-20231112213632435](https://cdncode.oss-cn-beijing.aliyuncs.com/test/202312292336692.png)

---



### 3.数据库字段连通性测试，为逻辑实现做准备（:new: :新插件   ）



- 1.创建相对应的测试类



![image-20231112214217201](https://cdncode.oss-cn-beijing.aliyuncs.com/test/202311122142988.png)

:imp:

**注意：**目录要**同级**才可以，防止报错！！！

![image-20231112214118694](https://cdncode.oss-cn-beijing.aliyuncs.com/test/202312292336693.png)

- 测试类对应代码 (:new:插件的使用：**generate插件**   )   

```java
package com.example.usercenter.service;
import java.util.Date;

import com.example.usercenter.model.domain.User;
import org.junit.jupiter.api.Test;
import org.springframework.boot.test.context.SpringBootTest;

import javax.annotation.Resource;

import static org.junit.jupiter.api.Assertions.*;
@SpringBootTest
public class UserServiceTest {

    @Resource
    private UserService userService;

    @Test
    public void testAddUser(){
       User user = new User();
        右键----->>>使用generate插件快速生成
        
       user.setUserName("ybh007");
       user.setUserAccount("admin");
       user.setAvatarUrl("https://cdncode.oss-cn-beijing.aliyun\n" +
               "cs.com/test/202311121922568.jpg");
       user.setGender(0);
       user.setUserPassword("131422");
       user.setPhone("18809459069");
       user.setEmail("1341433952@qq.com");
        boolean result = userService.save(user);
        System.out.println(user.getId());
       assertTrue(result);   //使用断言，判断最后得出的结果是否和你预期的一样


    }
}
```



![image-20231112214601435](https://cdncode.oss-cn-beijing.aliyuncs.com/test/202311122146248.png)

## 注册逻辑  :o:踩坑

1. 用户在前端输入账户和密码、以及校验码（todo）
2. 校验用户的账户、密码、校验密码，是否符合要求
   1. 非空
   2. 账户长度 **不小于** 4 位
   3. 密码就 **不小于** 8 位吧
   4. 账户不能重复
   5. 账户不包含特殊字符
   6. 密码和校验密码相同
3. 对密码进行加密（密码千万不要直接以明文存储到数据库中）
4. 向数据库插入用户数据



#### 测试报错 ！数据库字段下划线命名！:o:

```
#my-batis-plus驼峰配置 当数据库字段中有下划线是时，采用驼峰式命名，默认开启
#mybatis-plus:
#  configuration:
#    map-underscore-to-camel-case: false
```





![image-20231116184201531](https://cdncode.oss-cn-beijing.aliyuncs.com/test/202312292336694.png)

- 一定要使用和**数据库名一样的字段**名字
- ![image-20231116184342176](https://cdncode.oss-cn-beijing.aliyuncs.com/test/202312292336696.png)





## 登录功能

### 接口设计

接受参数：用户账户、密码

请求类型：POST 

请求体：JSON 格式的数据

> 请求参数很长时不建议用 get

返回值：用户信息（ **脱敏** ）



### 登录逻辑

1. 校验用户账户和密码是否合法

   1. 非空
   2. 账户长度不小于 4 位
   3. 密码就不小于 8 位
   4. 账户不包含特殊字符

2. 校验密码是否输入正确，要和数据库中的密文密码去对比

3. 用户信息脱敏，隐藏敏感信息，防止数据库中的字段泄露

4. 我们要记录用户的登录态（session），将其存到服务器上（用后端 SpringBoot 框架封装的服务器 tomcat 去记录）

   cookie

5. 返回脱敏后的用户信息

   

### 控制层Controller封装请求

```java
@RestController适用于编写result风格的api，返回值默认为json类型
```

 controller层倾向于请求参数本身的校验，不涉及业务逻辑本身

service层是对业务逻辑的校验（有可能被controller之外的层调用）

### 如何知道是哪个用户登录了？

> javaweb 这一块的知识

1. 连接服务器端后，得到一个 session 状态（匿名会话），返回给前端

2. 登录成功后，得到了登录成功的 session，并且给该sessio n设置一些值（比如用户信息），返回给前端一个设置 cookie 的 ”命令“ 

   **session => cookie** 

3. 前端接收到后端的命令后，设置 cookie，保存到浏览器内

4. 前端再次请求后端的时候（相同的域名），在请求头中带上cookie去请求

5. 后端拿到前端传来的 cookie，找到对应的 session

6. 后端从 session 中可以取出基于该 session 存储的变量（用户的登录信息、登录名）



### 查询用户是否存在:o:踩坑

#### 登录查询用户时候，删除的用户不查询（逻辑删除）

```yml
#配置
mybatis-plus:
	  global-config:
    db-config:
      logic-delete-field: id_delete # 全局逻辑删除的实体字段名(since 3.3.0,配置后可以忽略不配置步骤2)
      logic-delete-value: 1 # 逻辑已删除值(默认为 1)
      logic-not-delete-value: 0 # 逻辑未删除值(默认为 0)
```

```java
    /**
     * 是否删除
     */
    @TableLogic
    private Integer isDelete;
```



### 实现

控制层 Controller 封装请求

application.yml 指定接口全局路径前缀：

```
servlet:
  context-path: /api
```

控制器注解：

``` 
@RestController 适用于编写 restful 风格的 api，返回值默认为 json 类型
```

校验写在哪里？

- controller 层倾向于对请求参数本身的校验，不涉及业务逻辑本身（越少越好）
- service 层是对业务逻辑的校验（有可能被 controller 之外的类调用）

## 用户管理接口   

接口设计关键：必须鉴权！！！

1. 查询用户（允许根据用户名查询）
2. 删除用户



## 写代码流程

1. 先做设计
2. 代码实现
3. 持续优化！！！（复用代码、提取公共逻辑 / 常量）



## 前后端交互

前端需要向后端发送请求才能获取数据 / 执行操作。

怎么发请求：前端使用 ajax 来请求后端



### 前端请求库及封装关系

- axios 封装了 ajax

- request 是 ant design 项目又封装了一次



追踪 request 源码：用到了 umi 的插件、requestConfig 配置文件



## 代理:stars:代理和反向代理



![image-20240111164656119](C:\Users\prent\AppData\Roaming\Typora\typora-user-images\image-20240111164656119.png)

正向代理：替客户端向服务器发送请求，可以解决跨域问题

反向代理：替服务器统一接收请求。

怎么实现代理？

- Nginx 服务器
- Node.js 服务器

### 调试的使用

#### F8:下一步

#### F9:到下一断点





### 前端Ant_Design_pro搭建

1.使用命令安装框架

```
# 使用 npm
npm i @ant-design/pro-cli -g     //第一条命令
pro create myapp                 //第二条命令
```

2.使用**umi3**:star:

![image-20240111142128315](https://cdncode.oss-cn-beijing.aliyuncs.com/test/202401111421062.png)

3.选择模板

选择**simple**

4.安装依赖

```
$ cd myapp && tyarn
// 或
$ cd myapp && npm install
```

5.start启动项目

![image-20240111142347020](https://cdncode.oss-cn-beijing.aliyuncs.com/test/202401111423229.png)

如果版本太高，导致无法正确启动，则添加一行代码，即可运行

![在这里插入图片描述](https://img-blog.csdnimg.cn/1de67edb39f04823b4fac8b049a41af6.png)

```JS
windows中添加以下代码：
set NODE_OPTIONS=--openssl-legacy-provider 


linux Mac OS添加以下代码
export NODE_OPTIONS=--openssl-legacy-provider 
```

6.安装uni小工具

````
yarn add @umijs/preset-ui -D
````

### 前后端对接登录页面

**1.修改前端登录需要提交的表单信息**

#### 快捷键（批量修改全局名）：shift+F6

![image-20240111161254943](https://cdncode.oss-cn-beijing.aliyuncs.com/test/202401111613573.png)

**2.登录请求路径对接**

![image-20240111163625857](https://cdncode.oss-cn-beijing.aliyuncs.com/test/202401111636134.png)

3.解决跨域问题！！！（**端口号不一样就存在跨域**）

#### 使用代理

![image-20240111170543914](https://cdncode.oss-cn-beijing.aliyuncs.com/test/202401111705062.png)

**后端已经收到请求**

![image-20240111170612451](https://cdncode.oss-cn-beijing.aliyuncs.com/test/202401111706784.png)

4.修改前端结构

```tsx
 <ProFormText
      name="userAccount"
      fieldProps={{
        size: 'large',
        prefix: <UserOutlined className={styles.prefixIcon} />,
      }}
      placeholder='请输入账号'
      rules={[
        {
          required: true,
          message: '账号是必填项!'
        },
      ]}
    />
    <ProFormText.Password
      name="userPassword"
      fieldProps={{
        size: 'large',
        prefix: <LockOutlined className={styles.prefixIcon} />,
      }}
      placeholder='请输入密码'
      rules={[
        {
          required: true,
          message: '密码是必填项',
        },
        {
          min:8,
          type:'string',
          message:'长度不能小于8'
        },
      ]}
    />
  </>
)}
```

:sassy_man:至此前后端登录已经跑通，只是缺少页面的跳转

## 前端开发注册功能

1.根据登录路径找到对应的路由

2.添加一个页面

![image-20240111175046661](https://cdncode.oss-cn-beijing.aliyuncs.com/test/202401111750659.png)

3.通过路由，将网址和组件进行关联

![image-20240111175414043](https://cdncode.oss-cn-beijing.aliyuncs.com/test/202401111754638.png)

4.去除重定向（未登录则会跳转到登录页面），通过添加一个白名单的方式（后台管理系统的安全）

![image-20240111181250983](https://cdncode.oss-cn-beijing.aliyuncs.com/test/202401111812397.png)

### 创建前端管理页面

1.创建页面

![image-20240113130320629](https://cdncode.oss-cn-beijing.aliyuncs.com/test/202401131303760.png)

2.配置路由

![image-20240113130227263](https://cdncode.oss-cn-beijing.aliyuncs.com/test/202401131302347.png)

3.设置访问权限

![image-20240113130135556](https://cdncode.oss-cn-beijing.aliyuncs.com/test/202401131301120.png)

### 前段管理页加载内容（对接后端）

1.配置返回值

![image-20240120224314452](https://cdncode.oss-cn-beijing.aliyuncs.com/test/202401202243623.png)

2.设置返回的字段类型

![image-20240120224418427](https://cdncode.oss-cn-beijing.aliyuncs.com/test/202401202244940.png)

```java
 type CurrentUser = {
    id:number;
    userName: string;
    userAccount: string;
    avatarUrl:string;
    gender: number;
    phone: string;
    email: string;
    userStatus:number;
    userRole:number;
    createTime:Date;
  };
```

3.定义搜索用户接口

![image-20240120224738845](https://cdncode.oss-cn-beijing.aliyuncs.com/test/202401202247162.png)

4.接口与后端对应，返回查询到的用户列表

![image-20240120224937545](https://cdncode.oss-cn-beijing.aliyuncs.com/test/202401202249893.png)

5.前端管理页面有些字段不显示,解决方式如下

![image-20240120232609355](https://cdncode.oss-cn-beijing.aliyuncs.com/test/202401202326048.png)

**要和后端返回来的数据字段相对应**

```java
{
        "id": 1,
        "userName": "ybh007",
        "userAccount": "admin",
        "avatarUrl": "https://cdncode.oss-cn-beijing.aliyun\ncs.com/test/202311121922568.jpg",
        "gender": 0,
        "userPassword": null,
        "phone": "18809459069",
        "email": "1341433952@qq.com",
        "userStatus": 0,
        "createTime": "2023-11-12T13:25:31.000+00:00",
        "updateTime": null,
        "userRole": 1,
        "isDelete": null,
        "planetCode": null
    },
```

### 解决图片是字符串不加载问题！

![image-20240120233131589](https://cdncode.oss-cn-beijing.aliyuncs.com/test/202401202331806.png)

1.使用renser()函数进行渲染

```java
{
  title: '用户头像',
  dataIndex: 'avatarUrl',
  render:(_,record)=>(
  <div>
    <Image src={record.avatarUrl} width={100} />
  </div>
  ),
  copyable: true,
},
```

### 将枚举值转化为实际属性

```java
 {
    title: '用户角色',
    dataIndex: 'userRole',
    valueType:'select',
    valueEnum: {
      0: {text: '普通用户',status: 'Default'},
      1: {text: '管理员', status: 'Success',},
    },
    copyable: false,
  },
```

## :date:2024.1.23

1. 开发用户注销（前 / 后端）

2. 补充前后端注册校验逻辑（前 / 后端）
3. 后端代码优化
4. 前端代码优化
5. 项目部署上线
   - 买服务器
   - 原生部署
   - 容器部署
   - 绑定域名

:electric_plug:   插件Github Copilt（没啥用）

## 开发用户注销

前端操作

![image-20240123135948866](https://cdncode.oss-cn-beijing.aliyuncs.com/test/202401231359922.png)

#### 右上角图标不显示

![image-20240123141545508](https://cdncode.oss-cn-beijing.aliyuncs.com/test/202401231415115.png)

## 注册添加星球编号字段

> 仅适用于用户可相信的情况

用户自己填写：2~5位的编号

后台对编号进行校验，长度校验，唯一性校验

前端补充输入框，适配后端

> 后期拉取数据，定期清理违规用户

## 后端优化

1.通用返回对象

​	目的：给对象补充一些信息，告诉前端这个请求在业务层面上是成功还是失败

```java
{
	"name":yupi
}

/**
	成功
**/
{	"code":0 //业务状态码
		"data"{	
		"name"："jiefen"
		
		}
	"message":"ok"
}


/**
	错误
**/
{	"code":50001 //业务状态码
	"data"：null
	"message":"用户操作异常"
}



```

### 1.定义返回对象（Alt+insert快捷键 ：生成构造函数）

1.baseResponse类 :recycle:<T>泛型

构造函数的作用：

:star:方便创建对象

```java
<T>泛型的作用：什么类型的都也可以返回，提高了代码的复用性

@Data
public class BaseResponse<T> implements Serializable {

    private int code;

    private T data;

    private String message;
    
    //Alt+insert生成构造函数 ，方便
     public BaseResponse(int code, T data, String message) {
        this.code = code;
        this.data = data;
        this.message = message;
    }


}
```

2. new 对象，统一返回值

```java

        User user = userService.userLogin(userAccount, userPassword, request);
        return new BaseResponse<>(0,user,"ok");
```

 3.改进（使用 responseUtils 帮助我们来new对象）

```java
public class ResultUtils {
    public static <T> BaseResponse<T> success(T data){
        return new BaseResponse<>(0,data,"ok");
    }
} 
```

4.打重复的一段代码，使用idea工具template定义一个快捷键

site  ----->   Live-Templates  ----->  (定制快捷键)

![image-20240123205523980](https://cdncode.oss-cn-beijing.aliyuncs.com/test/202401232055033.png)

- 自定义错误码
- 返回类支持返回正常和错误

![image-20240123213711421](https://cdncode.oss-cn-beijing.aliyuncs.com/test/202401232137906.png)



### 2.封装全局异常处理

创建的原因：把所有的错误都在一个地方进行处理

1.定义业务异常类

​	1.在java类的异常类中，支持更多的字段

​	2.自定义构造函数，更灵活 / 快捷的设置字段

2.编写全局异常处理器

​	作用：

​		1.捕获代码中所有的异常，集中处理，让前端得到更详细的业务报错，

​		2.同时封装屏蔽掉项目框架本身的异常（不暴露服务器内部状态）

​	实现：	

   	1.SprinAop:在调用方法前后进行额外的处理

```java
@RestControllerAdvice  //sping的一个切面功能，在任意代码前后，进行封装
```

 



### 3.全局请求日志和登录校验(:railway_track: TO DO优化 )

## 前段优化（前端需要适配）

### 1.对接后端返回值，取 data 

