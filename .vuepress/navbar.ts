import {NavItem} from "vuepress/config";

export default [

    {
        text: "前端",
        link: '/前端/'

    },
    {
        text: "后端",
        link: '/后端/'
    },
    {
        text: "python",
        link: '/python/'
    },
    {
        text: "爬虫案例",
        link: '/爬虫案例/'
    },
    {
        text: "网络安全",
        link: '/网络安全/'
    },
    {
        text: "知识碎片",
        link: '/知识碎片/'
    },
    {
        text: "数据库",
        link: '/数据库/'
    },
    {
        text: "其他",
        link: '/其他/'
    },
] as NavItem[];
