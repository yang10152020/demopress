import {SidebarConfig4Multiple} from "vuepress/config";

import roadmapSideBar from "./sidebars/roadmapSideBar";
import frontSideBar from "./sidebars/frontSideBar";
import endSideBar from "./sidebars/endSideBar";
// @ts-ignore
export default {
    "/学习路线/": roadmapSideBar,
    "/前端/": frontSideBar,




    // 降级，默认根据文章标题渲染侧边栏
    "/": "auto",
} as SidebarConfig4Multiple;
